from pymongo import MongoClient
from TrainTestData.FLUO_TestData import fluoTestData
from TrainTestData.VIS_TestData import visTestData
from TrainTestData.NIR_TestData import nirTestData
import configproperties
import urllib.parse
from TrainTestData import IndividualValueToCsvWriter

if __name__ == '__main__':
    userName = urllib.parse.quote_plus(configproperties.mongoUserName)
    password = urllib.parse.quote_plus(configproperties.mongoPass)
    # client = MongoClient('mongodb://%s:%s@127.0.0.1' % (userName, password))
    client = MongoClient("localhost", 27017, maxPoolSize=50)

    db = client[configproperties.measurementsDbName]
    collection = db[configproperties.samplesCollectionName]

    cursor = collection.find({"useCase" : configproperties.uc1Name})

    count = 0

    for document in cursor:


        if count != 0 and count%50 == 0:
            print("")

        print(">", end="", flush=True)

        count += 1

        fluoTestData.createFluoTestData(document, configproperties.testDataOutputDir)
        visTestData.createVisTestData(document, configproperties.testDataOutputDir)
        nirTestData.createNirTestDataUc1(document, configproperties.testDataOutputDir)
        IndividualValueToCsvWriter.createAflaToCsv(document, configproperties.testDataOutputDir)

    print("")
    print(str(count))
