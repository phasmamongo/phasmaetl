import datetime
import logging
import os

import configproperties
from AveragingAndPreprocessing.FluoAverageModule import fluoAverageCsvProducer
from MongoDataLoading import uc2DataMongoLoader
from AveragingAndPreprocessing.NirAverageModule import uc2NirAverageCsvProducer
from AveragingAndPreprocessing.VisAverageModule import visAverageCsvProducer
from AveragingAndPreprocessing.VisPreprocessorModule import whiteDarkPreprocessor

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)

def main():
    logger.info("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                         :-3] + " Processing started for " + configproperties.uc2WorkingDir)

    allFolders = [os.path.relpath(x[0], configproperties.uc2WorkingDir) for x in os.walk(configproperties.uc2WorkingDir)]
    # allFolders = [x[0] for x in os.walk(configproperties.uc2WorkingDir)]

    insertedDocs = 0

    for folder in allFolders:

        if len(folder.split(os.sep)) == configproperties.lastPathTokenNum:
            logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                                  :-3] + " Working with " + folder)

            currentDir = os.path.join(configproperties.uc2WorkingDir, folder)
            sensorsSubFolders = [x[0] for x in os.walk(currentDir)]

            for sensorDir in sensorsSubFolders:

                relativeSensorDir = os.path.relpath(sensorDir, configproperties.uc2WorkingDir)

                if len(relativeSensorDir.split(os.sep)) == (configproperties.lastPathTokenNum + 1):
                    sensorName = os.path.relpath(sensorDir, currentDir)

                    if sensorName == configproperties.fluoSensorName:
                         logger.debug(" *  No PreProcessing enabled for " + configproperties.fluoSensorName)
                        # fluoAverageCsvProducer.produceAverage(sensorDir)

                    elif sensorName == configproperties.ftirSensorName:
                        # Do Nothing
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.ftirSensorName)

                    elif sensorName == configproperties.msiImgSensorName:
                        # Do Nothing
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.msiImgSensorName)

                    elif sensorName == configproperties.nirSensorName:
                         logger.debug(" *  No PreProcessing enabled for " + configproperties.nirSensorName)
                        # uc2NirAverageCsvProducer.produceAverage(sensorDir)

                    elif sensorName == configproperties.uvVisSensorName:
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.uvVisSensorName)
                        # visAverageCsvProducer.produceAverage(sensorDir)
                        # whiteDarkPreprocessor.processFolder(sensorDir)

                    elif sensorName == configproperties.microbFolderName:
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.microbFolderName)

                    elif sensorName == configproperties.aflatoxinFolderName:
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.aflatoxinFolderName)

                    else:
                        logger.error(" * Unknown Sensor Folder " + sensorName + ". Directory will not be processed: " + sensorDir )


            logger.info("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                                 :-3] + " Loading " + currentDir + " to Database...")

            # Call DB Scripts on sensorDir
            uc2DataMongoLoader.loadDataToMongo(currentDir)
            insertedDocs+= 1

            logger.info("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                                 :-3] + " Loading " + currentDir + " to Database... Done")

    logger.info(" >>>>> " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                            :-3] + " Total inserted Full Samples : " + str(insertedDocs))

if __name__ == "__main__":
    main()