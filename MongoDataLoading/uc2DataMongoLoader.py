import logging
import os
import configproperties
from MongoDataLoading import jnames
import urllib.parse
import sys
import datetime
from pathlib import Path
from pymongo import MongoClient
import json
from MongoDataLoading.SensorsToMongo.FLUO_Mongo import fluoToMongo
from MongoDataLoading.SensorsToMongo.FTIR_Mongo import ftirToMongo
from MongoDataLoading.SensorsToMongo.NIR_Mongo import nirToMongoAllFiles
from MongoDataLoading.SensorsToMongo.VIS_Mongo import visToMongoAllFiles
from MongoDataLoading.SensorsToMongo.MICRO_Mongo import microToMongo
from keyValuePairsMethod import deleteNullPairs

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)

# uc1WorkingDir=r"C:\Users\idaskalo\Desktop\UseCase1"

def loadDataToMongo(workWithDir):

    # logger=logging.getLogger(__name__)
    # logger.setLevel(logging.ERROR)

    # structureDir = os.path.relpath(workWithDir, topDir)
    userName = urllib.parse.quote_plus(configproperties.mongoUserName)
    password = urllib.parse.quote_plus(configproperties.mongoPass)
    # client = MongoClient('mongodb://%s:%s@127.0.0.1' % (userName, password)) # making a connection
    client = MongoClient(configproperties.mongoDbIp, configproperties.mongoDbPort)  # making a connection
    db = client.get_database(configproperties.measurementsDbName)  # getting a database (test_database))
    samples = db.get_collection(configproperties.samplesCollectionName)

    p = Path(workWithDir)

    useCase = configproperties.uc2Name
    laboratory = p.parts[-6]
    foodType = p.parts[-5]
    replicate = p.parts[-4]
    sample = p.parts[-3]
    temperature = p.parts[-2]
    exposureHours = p.parts[-1]

    vis = ""
    nir = ""
    microbiological = ""
    fluo = ""
    darkIntensity = ""
    dateTime = replicate.split(" ")
    format_str = '%d-%m-%Y'  # The format

    try:

        datetime_obj = datetime.datetime.strptime(dateTime[1], format_str)
    except:
        logger.warning(
            " * Exception in " + workWithDir + configproperties.wrongOrMissingDateErrorMsg)
        datetime_obj = ""

    for folder in os.listdir(workWithDir):

        if folder == configproperties.ftirSensorName:
            pathToFtir = os.path.join(workWithDir, folder)

            if os.listdir(pathToFtir):
                ftir, date, time = ftirToMongo.ftirToMongo1(workWithDir)

            else:
                logger.debug("There is no " + configproperties.ftirSensorName + " folder in directory: " + pathToFtir)

        elif folder == configproperties.fluoSensorName:

            pathToFluo = os.path.join(workWithDir, folder)

            if os.listdir(pathToFluo):
                logger.debug("The random numbers that have been chosen for test Data in FLUO are : ")
                fluo = fluoToMongo.fluoToMongoUc2(workWithDir)

            else:
                logger.debug("There is no " + configproperties.fluoSensorName + " folder in directory: " + pathToFluo)


        elif folder == configproperties.nirSensorName:

            pathToNir = os.path.join(workWithDir, folder)

            if os.listdir(pathToNir):
                logger.debug("The random numbers that have been chosen for test Data in NIR are : ")
                nir = nirToMongoAllFiles.uc2nirPlusAverageToMongo(workWithDir, configproperties.uc2NirAverageCsvProducerNirSkipRows)

            else:
                logger.debug("There no " + configproperties.nirSensorName + " folder in directory:" + pathToNir)


        elif folder == configproperties.uvVisSensorName:

            pathToUvVis = os.path.join(workWithDir, folder)

            if os.listdir(pathToUvVis):
                logger.debug("The random numbers that have been chosen for test Data in VIS are : ")
                vis = visToMongoAllFiles.uvVisPrepAndAverageToMongo(workWithDir)

            else:
                logger.debug("There is no " + configproperties.uvVisSensorName + " folder in directory: " + pathToUvVis)

        elif folder == configproperties.microbFolderName:
              pathToMicro = os.path.join(workWithDir,folder)

              if os.listdir(pathToMicro):
                  microbiological = {
                      jnames.microbiologicalUnit: configproperties.uc1MicrobUnit,
                      jnames.microbiologicalValue: microToMongo.microToMongo(workWithDir)
                  }
              else:
                  logger.debug("There is no " + configproperties.microbFolderName + " folder in directory: " + pathToMicro)



    sample={
                jnames.sample : sample,
                jnames.laboratory : laboratory,
                jnames.foodType : foodType,
                jnames.useCase : useCase,
                jnames.replicate : replicate,
                jnames.temperature : temperature,
                jnames.tempExposureHours : exposureHours,
                jnames.dateTime : datetime_obj,
                jnames.microbiologicalMeasurement : microbiological,

                configproperties.fluoSensorName : fluo,
                configproperties.uvVisSensorName : vis,
                configproperties.nirSensorName : nir

            }
    newSample = deleteNullPairs(sample)


    samples.insert_one(newSample)
