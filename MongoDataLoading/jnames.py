
#Afla
aflaToMongoName="name"
aflaToMongoValue= "value"
aflaToMongoUnit="unit"
aflaValue="value"

#Train/Test
trainData="trainData"
testData="testData"

columnsData = "columnsData"

#Waves
wavelengths="wavelengths"
wavenumbers="wavenumbers"
wavelength="wavelength"
wavelengthValue="wavelengthValue"

#Averages
averageDark="averageDark"
averageRef="averageRef"
averageMeasure="averageMeasurement"
average="average"
averageIntensity="averageIntensity"
averageAbsorbance="averageAbsorbance"
averageData="averageData"
overallAverages="overallAverages"

#Darks
darkIntensity="darkIntensity"
dark="dark"

#JSON
sample="sample"
laboratory="laboratory"
foodType= "foodType"
useCase="useCase"
replicate="replicate"
temperature="temperature"
tempExposureHours="tempExposureHours"
dateTime= "dateTime"
adultant = "adultant"
percentage = "percentage"
adulteration = "adulteration"
#UC3{
dateForFluo="dateForFluo"
dateForVis="dateForVis"
dateForNir="dateForNir"
#}

granularity="granularity"
contamination="contamination"
aflatoxin="aflatoxin"
experiment="experiment"
customType1="customType1"
customType2="customType2"
mainCat="mainCat"
cat="cat"
adul="adul"
rawDataRef="rawDataRef"

intensity="intensity"
absorbance="absorbance"

#Microbiological
microbiologicalMeasurement="microbiologicalMeasurement"
microbiologicalUnit="microbiologicalUnit"
microbiologicalValue="microbiologicalValue"

preprocessedValues="preprocessedValues"

measurements="measurements"

ref="ref"
whiteReference="white"
data="data"