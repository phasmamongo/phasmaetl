import os
import csv
import os.path
import logging

import sys

import configproperties

logging.basicConfig(level = configproperties.loggingLevel)
logger = logging.getLogger(__name__)

def ftirToMongo1(workWithDir):
 try:
        pathToFtir=os.path.join(workWithDir,configproperties.ftirSensorName)

        for filename in os.listdir(pathToFtir):

            ftirMeasurment = []

            # if the element is an CSV file then... Diaxorismos gia to FTIR measurment.

            if filename[-4:] == ".csv":

              pathToFtirFiles = os.path.join(pathToFtir, filename)
              with open(pathToFtirFiles, encoding="utf8") as f:

               csv_f = csv.reader(f)

               for i, row in enumerate(csv_f):

                 if i >= 19 and i <= 3755 and len(row) > 1:

                    ftirMeasurment.append({'wavelength': row[0],
                                           'wavelength_value': row[1]
                                           })

                 if i == 4 and len(row) > 1:

                  date = (row[1])

                 if i == 5 and len(row) > 1:

                  time = (row[1])

        return ftirMeasurment, date, time

 except:
     logger.error(" * Exception in " + workWithDir)
     logger.error(sys.exc_info()[0])
     ftirMeasurment=[]
     date=""
     time=""
     return ftirMeasurment,date,time


