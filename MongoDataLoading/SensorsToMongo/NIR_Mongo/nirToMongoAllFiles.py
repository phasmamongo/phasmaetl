import os
import csv
import os.path
import sys
import configproperties
import logging
import re
from MongoDataLoading import jnames
from keyValuePairsMethod import deleteNullPairs

from TrainTestData import trainTestDataSplitter

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)




def uc2nirPlusAverageToMongo(workWithDir, skiplines):
 try:
        intensity = []
        absorbance = []
        # averIntensities = []
        # averAbsorbances = []
        # wavenumbers = []
        whiteRef = []
        darkIntensity = ""
        data = []

        pathToNir = os.path.join(workWithDir, configproperties.nirSensorName)

        for filename in os.listdir(pathToNir):

            intensities = []
            absorbances = []
            # whiteRef = []




            if filename[-4:] == ".csv":

                pathToNirFiles = os.path.join(pathToNir, filename)

                with open(pathToNirFiles, encoding="utf8") as f:

                    csv_f = csv.reader(f)
                    # if filename == configproperties.averageFileName+configproperties.csvSuffix:
                    #     for i, row in enumerate(csv_f):
                    #
                    #         if i >= skiplines  and len(row) > 1:
                    #
                    #             averIntensities.append(row[1])
                    #             averAbsorbances.append(row[2])
                    #
                    #
                    #         else:
                    #             if i == configproperties.uc2NirDarkIntensityLine-1:
                    #                 darkIntensity = row[0].split("=")[1]
                    #                 # print(darkIntensity)

                    if filename[-5].isdigit():
                           # wavenumbers = []

                           for i, row in enumerate(csv_f):

                              if i >= skiplines and len(row) > 1:

                                 # wavenumbers.append(row[0])

                                 intensities.append(row[1])
                                 absorbances.append(row[2])


                              else:
                                  if i == configproperties.uc2NirDarkIntensityLine-1:
                                      darkIntensity = row[0].split("=")[1]
                                      # print(darkIntensity)

                           data.append({
                                         jnames.intensity : intensities,
                                         jnames.absorbance : absorbances
                                       })


                    elif filename.startswith(configproperties.uc2NirWhiteReferencePrefix):
                        for i, row in enumerate(csv_f):
                            whiteRef.append(row[1])
        # trainData,testData = trainTestDataSplitter.trainTestData(data)



        nirMeasurementNew = {
                               # jnames.wavenumbers : wavenumbers,
                               # jnames.trainData : trainData,
                               # jnames.testData : testData,
                               # jnames.averageIntensity : averIntensities,
                               # jnames.averageAbsorbance : averAbsorbances,
                               jnames.data : data,
                               jnames.whiteReference : whiteRef,
                               jnames.darkIntensity : darkIntensity
                            }
        newNirMeasurement = deleteNullPairs(nirMeasurementNew)
        return newNirMeasurement
 except :



     nirMeasurementNew = ""
     return nirMeasurementNew

def uc1nirPlusAverageToMongo(workWithDir, skiplines):
     try:
         nirMeasurement = []
         pathToNir = os.path.join(workWithDir, configproperties.nirSensorName)
         # average = []
         # wavenumbers = []
         countDigitFiles = 0
         whiteRefValues = []
         darkIntensity = ""
         for filename in os.listdir(pathToNir):


             values = []

             if filename[-4:] == ".csv":

                 pathToNirFiles = os.path.join(pathToNir, filename)

                 with open(pathToNirFiles, encoding="utf8") as f:

                     csv_f = csv.reader(f)
                     # if filename == configproperties.averageFileName + configproperties.csvSuffix:
                     #
                     #     for i, row in enumerate(csv_f):
                     #
                     #         if i >= skiplines and len(row) > 1:
                     #
                     #             average.append(row[1])

                     if filename[-5].isdigit():
                         countDigitFiles += 1
                         # wavenumbers = []
                         for i, row in enumerate(csv_f):

                              if i >= skiplines and len(row) > 1:
                                 # wavenumbers.append(row[0])
                                 values.append(row[1])
                         nirMeasurement.append(values)

                     elif filename.startswith(configproperties.uc1NirWhiteReferencePrefix) :
                         for i, row in enumerate(csv_f):

                              if i >= skiplines and len(row) > 1:
                                  whiteRefValues.append(row[1])


         # trainData , testData = trainTestDataSplitter.trainTestData(nirMeasurement)


         nirMeasurementNew = {
                              # jnames.wavenumbers : wavenumbers,
                              # jnames.trainData : trainData,
                              # jnames.testData : testData,
                              # jnames.average : average,
                              jnames.data : nirMeasurement,
                              jnames.whiteReference : whiteRefValues,
                              jnames.darkIntensity: darkIntensity
                          }
         newNirMeasurement = deleteNullPairs(nirMeasurementNew)
         return newNirMeasurement
     except :



         nirMeasurementNew = ""
         return nirMeasurementNew
