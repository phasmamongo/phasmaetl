import os
import csv
import os.path
import logging
import sys
from MongoDataLoading import jnames
import configproperties
from TrainTestData import csvFileColumnsSplitter
from TrainTestData import  trainTestDataSplitter
from keyValuePairsMethod import deleteNullPairs

logging.basicConfig(level = configproperties.loggingLevel)
logger = logging.getLogger(__name__)


def uvVisPrepAndAverageToMongo(workWithDir):
 try:
        data = []
        white = []
        dark = []
        # wavelengths = []
        # visAverage = []
        # preprocessed = []
        pathToUvVis = os.path.join(workWithDir, configproperties.uvVisSensorName)
        # print(pathToUvVis)

        sList = os.listdir(pathToUvVis)

        for filename in sList:


            # averageMeasure = []
            # averageDark = []
            # averageRef = []



            if filename[-4:] == ".csv":

                if filename[-5].isdigit():

                    pathToUvVisFile = os.path.join(pathToUvVis, filename)
                    pathToUvVisDark = os.path.join(pathToUvVis, filename[:-4] + configproperties.darkSuffix + configproperties.csvSuffix)
                    pathToUvVisRef = os.path.join(pathToUvVis, filename[:-4] + configproperties.refSuffix + configproperties.csvSuffix)

                    columnsData= csvFileColumnsSplitter.columnsSplitter(pathToUvVisFile)
                    # , wavelengths, averages
                    columnsDataDark = csvFileColumnsSplitter.columnsSplitter(pathToUvVisDark)
                    # , wavelengthsDark, averagesDark
                    columnsDataRef = csvFileColumnsSplitter.columnsSplitter(pathToUvVisRef)
                    # , wavelengthsRef, averagesRef

                    # vis = {
                    #
                    #     jnames.columnsData : {
                    #         jnames.measurements : columnsData,
                    #         jnames.dark : columnsDataDark,
                    #         jnames.ref : columnsDataRef
                    #     },

                        # jnames.averageData : {
                        #     jnames.measurements : averages,
                        #     jnames.dark : averagesDark,
                        #     jnames.ref : averagesRef
                        # }

                    # }

                    data.append(columnsData)
                    white.append(columnsDataRef)
                    dark.append(columnsDataDark)
                # if filename == configproperties.averageFileName + configproperties.csvSuffix:
                #
                #     pathToUvVisAverageFile = os.path.join(pathToUvVis, filename)
                #     pathToUvVisAverageDark = os.path.join(pathToUvVis, filename[:-4] + configproperties.darkSuffix + configproperties.csvSuffix)
                #     pathToUvVisAverageRef = os.path.join(pathToUvVis, filename[:-4] + configproperties.refSuffix + configproperties.csvSuffix)
                #
                #     with open(pathToUvVisAverageFile, encoding="utf8") as f:
                #
                #         csv_f = csv.reader(f)
                #
                #         for i, row in enumerate(csv_f):
                #
                #             if i > 0:
                #                 averageMeasure.append( row[1])
                #
                #         # print(measure)
                #
                #     with open(pathToUvVisAverageDark, encoding="utf8") as f:
                #
                #         csv_f1 = csv.reader(f)
                #
                #         for i, row in enumerate(csv_f1):
                #
                #             if i > 0:
                #                 averageDark.append(row[1])
                #
                #     with open(pathToUvVisAverageRef, encoding="utf8") as f:
                #
                #         csv_f2 = csv.reader(f)
                #
                #         for i, row in enumerate(csv_f2):
                #
                #             if i > 0:
                #                 averageRef.append(row[1])
                #
                #     visAverage = {
                #         jnames.averageDark : averageDark,
                #         jnames.averageRef : averageRef,
                #         jnames.averageMeasure : averageMeasure
                #     }
                #
                # if filename == configproperties.whiteDarkPreprocessorOutFilePrefix + configproperties.averageFileName + configproperties.csvSuffix:
                #     pathToUvVisPreprocessedFile = os.path.join(pathToUvVis, filename)
                #
                #     with open(pathToUvVisPreprocessedFile, encoding="utf8") as f:
                #
                #         csv_f1 = csv.reader(f)
                #
                #         for i, row in enumerate(csv_f1):
                #             if i > 0:
                #                 preprocessed.append(row[1])

        # trainData, testData = trainTestDataSplitter.trainTestData(measures)

        measuresNew = {

            jnames.data : data,
            jnames.whiteReference : white,
            jnames.dark: dark
            # jnames.wavelengths : wavelengths,
            # jnames.measurements :{

                # "trainData" : trainData,
                # "testData" : testData
            # } ,
            # jnames.overallAverages : visAverage,
            # jnames.preprocessedValues : preprocessed
        }
        newMeasurements = deleteNullPairs(measuresNew)

        return newMeasurements

 except:

     logger.error(" * Exception in " + workWithDir)
     logger.error(sys.exc_info()[0])
     measuresNew = ""

     return measuresNew
