import os
import csv
import re
import os.path
import sys
import configproperties
import logging
from MongoDataLoading import jnames
from TrainTestData import csvFileColumnsSplitter
from TrainTestData import  trainTestDataSplitter
from keyValuePairsMethod import deleteNullPairs

logging.basicConfig(level = configproperties.loggingLevel)
logger = logging.getLogger(__name__)


def FluoToMongo(workWithDir):

     try:

            # averageMeasurement = []
            # wavelengths = []
            # averagesDark = []
            # averagesRef = []
            data = []
            dark = []
            white = []
            # fluoAllFilesMeasurements = []
            columnsDataDark = []
            columnsDataRef = []




            pathToFluo=os.path.join(workWithDir,configproperties.fluoSensorName)

            for filename in os.listdir(pathToFluo):

                if filename[-4:] == ".csv":

                    pathToFluoFiles = os.path.join(pathToFluo, filename)
                    with open(pathToFluoFiles, encoding="utf8") as f:

                        if re.split("[_.]", filename)[-2] == "Dark":

                             columnsDataDark = csvFileColumnsSplitter.columnsSplitter(pathToFluoFiles)
                             # , wavelengths, averagesDark
                             dark.append(columnsDataDark)


                        if re.split("[_.]", filename)[-2] == "ref":
                            columnsDataRef = csvFileColumnsSplitter.columnsSplitter(pathToFluoFiles)
                            # , wavelengths, averagesRef
                            white.append(columnsDataRef)
            for filename in os.listdir(pathToFluo):

                if filename[-4:] == ".csv":

                    pathToFluoFiles = os.path.join(pathToFluo, filename)
                    with open(pathToFluoFiles, encoding="utf8") as f:

                        csv_f = csv.reader(f)

                        if re.split("[_.]",filename)[-2].isdigit():


                            columnsData = csvFileColumnsSplitter.columnsSplitter(pathToFluoFiles)
                            # , wavelengths, averages
                            # fluoFileMeasurements={
                            #                   jnames.columnsData:{
                            #                                      jnames.measurements : columnsData,
                            #                                      jnames.dark : columnsDataDark,
                            #                                      jnames.ref : columnsDataRef
                            #                                    },
                            #
                            #                   jnames.averageData: {
                            #                                         jnames.measurements : averages,
                            #                                         jnames.dark : averagesDark,
                            #                                         jnames.ref : averagesRef
                            #                                       }
                            #                   }
                            #
                            # fluoAllFilesMeasurements.append(fluoFileMeasurements)
                            data.append(columnsData)

                        # elif filename == configproperties.averageFileName + configproperties.csvSuffix :
                        #       for i, row in enumerate(csv_f):
                        #           if i>0:
                        #            averageMeasurement.append(row[1])

            # trainData, testData = trainTestDataSplitter.trainTestData(fluoAllFilesMeasurements)

            fluo = {
                jnames.data: data,
                jnames.whiteReference: white,
                jnames.dark : dark
                # jnames.wavelengths : wavelengths,
                # jnames.measurements : {
                #                         "trainData" : trainData,
                #                         "testData": testData
                #                         } ,
                #
                # jnames.overallAverages : averageMeasurement
            }
            newFluo = deleteNullPairs(fluo)


            return newFluo
     except:
         logger.error(" * Exception in " + workWithDir)
         logger.error(sys.exc_info()[0])
         fluo = ""
         return fluo


def fluoToMongoUc2(workWithDir):

    try:
        # measures = []
        # wavelengths = []
        data = []
        dark = []
        white = []
        # fluoAverage = []
        pathToFluo = os.path.join(workWithDir, configproperties.fluoSensorName)

        sList = os.listdir(pathToFluo)

        for filename in sList:

            # averageMeasure = []


            if filename[-4:] == ".csv":

                if filename[-5].isdigit():
                    pathToFluoFile = os.path.join(pathToFluo, filename)
                    pathToFluoDark = os.path.join(pathToFluo, filename[:-4] + configproperties.darkSuffix + configproperties.csvSuffix)
                    pathToFluoRef = os.path.join(pathToFluo, filename[:-4] + configproperties.refSuffix + configproperties.csvSuffix)

                    columnsData = csvFileColumnsSplitter.columnsSplitter(pathToFluoFile)
                    # , wavelengths, averages
                    columnsDataDark = csvFileColumnsSplitter.columnsSplitter(pathToFluoDark)
                    # , wavelengthsDark, averagesDark
                    columnsDataRef = csvFileColumnsSplitter.columnsSplitter(pathToFluoRef)
                    # , wavelengthsRef, averagesRef

                    # fluo_measurement = {
                    #
                    #     jnames.columnsData: {
                    #         jnames.measurements: columnsData,
                    #         jnames.dark: columnsDataDark,
                    #         jnames.ref: columnsDataRef
                    #     },
                    #
                    #     jnames.averageData: {
                    #         jnames.measurements: averages,
                    #         jnames.dark: averagesDark,
                    #         jnames.ref: averagesRef
                    #     }
                    #
                    # }

                    # measures.append(fluo_measurement)

                    data.append(columnsData)
                    dark.append(columnsDataDark)
                    white.append(columnsDataRef)

                # if filename == configproperties.averageFileName + configproperties.csvSuffix:
                #
                #     pathToFluoAverageFile = os.path.join(pathToFluo, filename)
                #
                #     with open(pathToFluoAverageFile, encoding="utf8") as f:
                #
                #         csv_f = csv.reader(f)
                #
                #         for i, row in enumerate(csv_f):
                #
                #             if i > 0:
                #                 averageMeasure.append(row[1])
                #
                #
                #
                #     fluoAverage = {
                #
                #         jnames.averageMeasure: averageMeasure
                #     }


        # trainData, testData = trainTestDataSplitter.trainTestData(measures)

        fluo = {

            jnames.data : data,
            jnames.whiteReference : white,
            jnames.dark : dark
            # jnames.wavelengths: wavelengths,
            # jnames.measurements: {
            #
            #     "trainData": trainData,
            #     "testData": testData
            # },
            # jnames.overallAverages: fluoAverage,
        }
        newFluo = deleteNullPairs(fluo)

        return newFluo

    except:

        logger.error(" * Exception in " + workWithDir)
        logger.error(sys.exc_info()[0])
        fluo = ""

        return fluo

