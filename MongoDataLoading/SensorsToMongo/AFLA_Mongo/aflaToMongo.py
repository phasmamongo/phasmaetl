import os
import csv
import configproperties
import logging
import sys

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)
from MongoDataLoading import jnames


def aflaToMongo(workingDir):
    try:
        pathToAfla = os.path.join(workingDir, configproperties.aflatoxinFolderName)
        for filename in os.listdir(pathToAfla):
            if filename[-4:] == configproperties.csvSuffix:
                name = filename[:-4]
                pathToAflaFile = os.path.join(pathToAfla, filename)

                with open(pathToAflaFile, encoding="utf8") as f:
                    csv_f = csv.reader(f)

                    for i, row in enumerate(csv_f):
                        if i == 1:

                            quantification = row[1]
                            unit = row[2]

                            afla = {
                                jnames.aflaToMongoName: name,
                                jnames.aflaToMongoValue: quantification,
                                jnames.aflaToMongoUnit: unit
                            }

        return afla

    except:
        logger.error(" * Exception in " + workingDir)
        logger.error(sys.exc_info()[0])
        afla = ""
        return afla