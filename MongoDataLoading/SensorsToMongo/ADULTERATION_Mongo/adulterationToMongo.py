import logging
import sys
import configproperties
from pathlib import Path
from MongoDataLoading import jnames
logging.basicConfig(level = configproperties.loggingLevel)
logger = logging.getLogger(__name__)

def adulterationMongo(workWithDir) :
    try:
        adulteration = []
        p = Path(workWithDir)
        foodType = p.parts[-4]
        adult = p.parts[-1]
        adultToken = adult.split("_")
        length = adultToken.__len__()

        if adultToken[0].isdigit() and adultToken[1] == foodType.lower():
            for i in range(length - 2):
                if i % 2 != 0:
                    percentage = adultToken[i + 1]
                    adultant = adultToken[i + 2]
                    adulterationKeyValue = {
                        jnames.adultant: adultant,
                        jnames.percentage: percentage
                    }
                    adulteration.append(adulterationKeyValue)

        # else:
        #     if adultToken[0] == foodType.lower():
        #         adultant = "none"
        #         percentage = str(0)
        #
        #     else:
        #         adultant = adultToken[0]
        #         percentage = str(100)
        #
        #     adulterationKeyValue = {
        #         jnames.adultant: adultant,
        #         jnames.percentage: percentage
        #     }
        #     adulteration.append(adulterationKeyValue)

        return adulteration
    except:
        logger.error(" * Exception in " + workWithDir)
        logger.error(sys.exc_info()[0])
        adulteration = ""
        return adulteration
