import logging
import os
import os.path

import sys

import configproperties

logging.basicConfig(level = configproperties.loggingLevel)
logger = logging.getLogger(__name__)

def msiImgToMongo(workWithDir):

    try:
        pathToImg=os.path.join(workWithDir,configproperties.msiImgSensorName)


        for filename3 in os.listdir(pathToImg):

            pathToImgFiles=os.path.join(pathToImg,filename3)
            datafile = open(pathToImgFiles, "rb");
            thedata = datafile.read()

        img={"Image name":filename3,
             'Binary':thedata
             }

        return img

    except:
        logger.error(" * Exception in " + workWithDir)
        logger.error(sys.exc_info()[0])
        img = {"Image name": "", "Binary" : ""}
        return img