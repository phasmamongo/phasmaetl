import csv
import os
import os.path
import logging

import sys

import configproperties

logging.basicConfig(level = configproperties.loggingLevel)
logger = logging.getLogger(__name__)

def microToMongo(workWithDir):
  try:
        pathToMicro = os.path.join(workWithDir, configproperties.microbFolderName)

        for filename in os.listdir(pathToMicro):


            if filename[-4:]==".csv":

                pathToMicroFile = os.path.join(pathToMicro, filename)
                with open(pathToMicroFile,encoding="utf8") as f:
                    csv_f=csv.reader(f)
                    for i,row in enumerate(csv_f):
                     microMeasurement=row[2]

        return microMeasurement
  except:
      logger.error(" * Exception in " + workWithDir)
      logger.error(sys.exc_info()[0])
      microMeasurement=""
      return microMeasurement
