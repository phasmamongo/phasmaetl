import csv
import urllib.parse
import logging
import os
import configproperties
from MongoDataLoading import jnames
import urllib.parse
import sys
import datetime
from pathlib import Path
from pymongo import MongoClient
import json
from MongoDataLoading.SensorsToMongo.FLUO_Mongo import fluoToMongo
from MongoDataLoading.SensorsToMongo.FTIR_Mongo import ftirToMongo
from MongoDataLoading.SensorsToMongo.NIR_Mongo import nirToMongoAllFiles
from MongoDataLoading.SensorsToMongo.VIS_Mongo import visToMongoAllFiles
from MongoDataLoading.SensorsToMongo.MICRO_Mongo import microToMongo
from keyValuePairsMethod import deleteNullPairs
from MongoDataLoading.SensorsToMongo.ADULTERATION_Mongo import adulterationToMongo


def loadDataToMongo(workingDir):


    logging.basicConfig(level=configproperties.loggingLevel)
    logger = logging.getLogger(__name__)
    userName = urllib.parse.quote_plus(configproperties.mongoUserName)
    password = urllib.parse.quote_plus(configproperties.mongoPass)
    # client = MongoClient('mongodb://%s:%s@127.0.0.1' % (userName, password))
    client = MongoClient(configproperties.mongoDbIp, configproperties.mongoDbPort)  # making a connection
    db = client.get_database(configproperties.measurementsDbName)  # getting a database (test_database))
    csvSamples = db.get_collection(configproperties.samplesCollectionName)

    for filename in os.listdir(workingDir):
     # print(filename)
     if filename[-4:] == ".csv":

         if filename[-7:-4] == "NIR":
             pathToNir = os.path.join(workingDir,filename)
             # print(pathToNir)
         if filename[-7:-4] == "VIS":
            pathToVis = os.path.join(workingDir,filename)
            # print(pathToVis)
         if filename[-8:-4] == "FLUO":
             pathToFluo = os.path.join(workingDir,filename)
             # print(pathToFluo)

    with open(pathToFluo,encoding="utf8") as fluoCsv :
        with open(pathToVis,encoding="utf8") as visCsv :
           with open(pathToNir,encoding="utf8") as nirCsv :

               fluoReader = csv.reader(fluoCsv)
               visReader = csv.reader(visCsv)
               nirReader = csv.reader(nirCsv)
               fluoWavelengthValues = []
               visWavelengthValues = []
               nirWavelengthValues = []
               mainCat = []
               cat = []
               adul = []
               replicate = []
               sampleId = []
               rawDataRef = []
               calibrationFileRef = []
               dateFluo = []
               dateVis = []
               dateNir = []
               customType1 = ""
               customType2 = ""
               lab = ""
               product = ""
               nirWavelength=""
               wavelength=""

               for rowNum, currentFluoRow in enumerate(fluoReader) :

                   if rowNum == 0 :
                       wavelength = currentFluoRow[13:]
                       # print(wavelength)
                   else:
                       if rowNum == 1 :
                            lab = currentFluoRow[0]
                            product = currentFluoRow[1]
                            customType1 = currentFluoRow[2]
                            customType2 = currentFluoRow[3]
                            # sensor = currentFluoRow[4]

                            # print(lab,product,sensor,customType1,customType2)
                       dateFluo.append(currentFluoRow[5])
                       mainCat.append(currentFluoRow[6])
                       cat.append(currentFluoRow[7])
                       adul.append(currentFluoRow[8])
                       replicate.append(currentFluoRow[9])
                       sampleId.append(currentFluoRow[10])
                       rawDataRef.append(currentFluoRow [11])
                       calibrationFileRef.append(currentFluoRow[12])
                       fluoWavelengthValues.append(currentFluoRow[13:])

               # print(len(fluoWavelengthValues))

               for rowNum, currentVisRow in enumerate(visReader):

                   if rowNum != 0:
                       dateVis.append(currentVisRow[5])
                       visWavelengthValues.append(currentVisRow[13:])

               # print(len(visWavelengthValues))

               for rowNum, currentNirRow in enumerate(nirReader):

                   if rowNum == 0:
                       nirWavelength = currentNirRow[13:]
                   else:
                       dateNir.append(currentNirRow[5])
                       nirWavelengthValues.append(currentNirRow[13:])

               # print(nirWavelengthValues[0])


               samples = len(visWavelengthValues) # Bulk  156  ||||  chemical  136

               measurementsNumber = len(wavelength) #288 ||| 288

               measurementsForNir = len(nirWavelength) # 901 ||| 901

               for sample in range(samples):
                    fluoMeasurement = []
                    visMeasurement = []
                    nirMeasurement = []
                    for measurement in range(measurementsNumber):
                       fluoMeasurement.append({
                                                jnames.wavelength : wavelength[measurement],
                                                jnames.wavelengthValue : fluoWavelengthValues[sample][measurement]
                                              })

                       visMeasurement.append({
                                               jnames.wavelength : wavelength[measurement],
                                               jnames.wavelengthValue : visWavelengthValues[sample][measurement]
                                             })
                    # print("Measurements for fluo and sample :" + sampleId[sample])
                    # print(fluoMeasurement )
                    # print("Measurements for vis and sample :" + sampleId[sample])
                    # print(visMeasurement)

                    for measurementNir in range(measurementsForNir):
                       nirMeasurement.append({
                                                jnames.wavelength : nirWavelength[measurementNir],
                                                jnames.wavelengthValue : nirWavelengthValues[sample][measurementNir]
                                             })
                    # print("Measurements for nir and sample :" + sampleId[sample])
                    # print(nirMeasurement)

                    jsonSample = {
                                    jnames.sample : sampleId[sample],
                                    jnames.laboratory : lab,
                                    jnames.foodType : product,
                                    jnames.useCase : configproperties.uc3Name,
                                    jnames.replicate : replicate[sample],
                                    jnames.temperature : "" ,
                                    jnames.tempExposureHours : "",
                                    jnames.dateTime : {
                                                      jnames.dateForFluo :dateFluo[sample],
                                                      jnames.dateForVis : dateVis[sample],
                                                      jnames.dateForNir : dateNir[sample]
                                                  },

                                    jnames.customType1 : customType1,
                                    jnames.customType2 : customType2,
                                    jnames.mainCat : mainCat[sample],
                                    jnames.cat : cat[sample],
                                    jnames.adul : adul[sample],
                                    jnames.rawDataRef : rawDataRef[sample],
                                    jnames.experiment: {
                                                          configproperties.fluoSensorName : fluoMeasurement,
                                                          configproperties.uvVisSensorName : visMeasurement,
                                                          configproperties.nirSensorName : nirMeasurement,
                                                       }
                                 }

                    csvSamples.insert_one(jsonSample)





logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)

# uc1WorkingDir=r"C:\Users\idaskalo\Desktop\UseCase1"

def loadDataToMongoUseCase2(workWithDir):

    # logger=logging.getLogger(__name__)
    # logger.setLevel(logging.ERROR)

    # structureDir = os.path.relpath(workWithDir, topDir)
    userName = urllib.parse.quote_plus(configproperties.mongoUserName)
    password = urllib.parse.quote_plus(configproperties.mongoPass)
    # client = MongoClient('mongodb://%s:%s@127.0.0.1' % (userName, password)) # making a connection
    client = MongoClient(configproperties.mongoDbIp, configproperties.mongoDbPort)  # making a connection
    db = client.get_database(configproperties.measurementsDbName)  # getting a database (test_database))
    samples = db.get_collection(configproperties.samplesCollectionName)

    p = Path(workWithDir)

    useCase = configproperties.uc3Name
    laboratory = p.parts[-6]
    foodType = p.parts[-4]
    # adulterationType = p.parts[-4]
    replicate = p.parts[-3]
    sample = p.parts[-2]
    # adulteration = p.parts[-1]
    # adulterationToken = adulteration.split("_")
    # adulteratedWith = ""
    vis = ""
    nir = ""
    fluo = ""
    darkIntensity = ""
    dateTime = replicate.split(" ")
    format_str = '%d-%m-%Y'  # The format

    try:

        datetime_obj = datetime.datetime.strptime(dateTime[1], format_str)
    except:
        logger.warning(
            " * Exception in " + workWithDir + configproperties.wrongOrMissingDateErrorMsg)
        datetime_obj = ""

    for folder in os.listdir(workWithDir):

        if folder == configproperties.ftirSensorName:
            pathToFtir = os.path.join(workWithDir, folder)

            if os.listdir(pathToFtir):
                ftir, date, time = ftirToMongo.ftirToMongo1(workWithDir)

            else:
                logger.debug("There is no " + configproperties.ftirSensorName + " folder in directory: " + pathToFtir)

        elif folder == configproperties.fluoSensorName:

            pathToFluo = os.path.join(workWithDir, folder)

            if os.listdir(pathToFluo):
                logger.debug("The random numbers that have been chosen for test Data in FLUO are : ")
                fluo = fluoToMongo.fluoToMongoUc2(workWithDir)

            else:
                logger.debug("There is no " + configproperties.fluoSensorName + " folder in directory: " + pathToFluo)


        elif folder == configproperties.nirSensorName:

            pathToNir = os.path.join(workWithDir, folder)

            if os.listdir(pathToNir):
                logger.debug("The random numbers that have been chosen for test Data in NIR are : ")
                nir = nirToMongoAllFiles.uc2nirPlusAverageToMongo(workWithDir, configproperties.uc2NirAverageCsvProducerNirSkipRows)

            else:
                logger.debug("There no " + configproperties.nirSensorName + " folder in directory:" + pathToNir)


        elif folder == configproperties.uvVisSensorName:

            pathToUvVis = os.path.join(workWithDir, folder)

            if os.listdir(pathToUvVis):
                logger.debug("The random numbers that have been chosen for test Data in VIS are : ")
                vis = visToMongoAllFiles.uvVisPrepAndAverageToMongo(workWithDir)

            else:
                logger.debug("There is no " + configproperties.uvVisSensorName + " folder in directory: " + pathToUvVis)

        # elif folder == configproperties.microbFolderName:
        #       pathToMicro = os.path.join(workWithDir,folder)
        #
        #       if os.listdir(pathToMicro):
        #           micro = microToMongo.microToMongo(workWithDir)
        #       else:
        #           logger.debug("There is no " + configproperties.microbFolderName + " folder in directory: " + pathToMicro)

    adulteration = adulterationToMongo.adulterationMongo(workWithDir)


    sample={
                jnames.sample : sample,
                jnames.laboratory : laboratory,
                jnames.foodType : foodType,
                jnames.useCase : useCase,
                jnames.replicate : replicate,
                jnames.adulteration : adulteration,
                configproperties.fluoSensorName : fluo,
                configproperties.uvVisSensorName : vis,
                configproperties.nirSensorName : nir

            }
    newSample = deleteNullPairs(sample)


    samples.insert_one(newSample)
