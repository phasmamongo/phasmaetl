import datetime
import fnmatch
import logging
import os

import configproperties
from MongoDataLoading import uc3DataMongoLoader


logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)


def main():
    #TODO THIS WAS FOR YANNICKS MEASUREMENTS
    # logger.info("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
    #                      :-3] + " Processing started for " + configproperties.uc3WorkingDir)
    #
    # processedDirs = 0
    #
    # for item in os.listdir(configproperties.uc3WorkingDir):
    #     if os.path.isdir(os.path.join(configproperties.uc3WorkingDir, item)):
    #
    #         workingDir = os.path.join(configproperties.uc3WorkingDir, item)
    #
    #         logger.info("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
    #                              :-3] + " Loading " + workingDir + " to Database...")
    #
    #         # Call DB Scripts for all csv files in dir
    #         uc3DataMongoLoader.loadDataToMongoU(workingDir)
    #         processedDirs += 1
    #
    #         logger.info("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
    #                          :-3] + " Loading to Database... Done")
    #
    # logger.info(" >>>>> " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
    #                         :-3] + " Total processed directories : " + str(processedDirs))
    logger.info("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                         :-3] + " Processing started for " + configproperties.uc3WorkingDir)

    allFolders = [os.path.relpath(x[0], configproperties.uc3WorkingDir) for x in
                  os.walk(configproperties.uc3WorkingDir)]
    # allFolders = [x[0] for x in os.walk(configproperties.uc2WorkingDir)]

    insertedDocs = 0

    for folder in allFolders:

        if len(folder.split(os.sep)) == configproperties.lastPathTokenNum:
            logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                                  :-3] + " Working with " + folder)

            currentDir = os.path.join(configproperties.uc3WorkingDir, folder)
            sensorsSubFolders = [x[0] for x in os.walk(currentDir)]

            for sensorDir in sensorsSubFolders:

                relativeSensorDir = os.path.relpath(sensorDir, configproperties.uc3WorkingDir)

                if len(relativeSensorDir.split(os.sep)) == (configproperties.lastPathTokenNum + 1):
                    sensorName = os.path.relpath(sensorDir, currentDir)

                    if sensorName == configproperties.fluoSensorName:
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.fluoSensorName)
                    # fluoAverageCsvProducer.produceAverage(sensorDir)

                    elif sensorName == configproperties.ftirSensorName:
                        # Do Nothing
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.ftirSensorName)

                    elif sensorName == configproperties.msiImgSensorName:
                        # Do Nothing
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.msiImgSensorName)

                    elif sensorName == configproperties.nirSensorName:
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.nirSensorName)
                    # uc2NirAverageCsvProducer.produceAverage(sensorDir)

                    elif sensorName == configproperties.uvVisSensorName:
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.uvVisSensorName)
                        # visAverageCsvProducer.produceAverage(sensorDir)
                        # whiteDarkPreprocessor.processFolder(sensorDir)

                    elif sensorName == configproperties.microbFolderName:
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.microbFolderName)

                    elif sensorName == configproperties.aflatoxinFolderName:
                        logger.debug(" *  No PreProcessing enabled for " + configproperties.aflatoxinFolderName)

                    else:
                        logger.error(
                            " * Unknown Sensor Folder " + sensorName + ". Directory will not be processed: " + sensorDir)

            logger.info("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                                 :-3] + " Loading " + currentDir + " to Database...")

            # Call DB Scripts on sensorDir
            uc3DataMongoLoader.loadDataToMongoUseCase2(currentDir)
            insertedDocs += 1

            logger.info("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                                 :-3] + " Loading " + currentDir + " to Database... Done")

    logger.info(" >>>>> " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                            :-3] + " Total inserted Full Samples : " + str(insertedDocs))

if __name__ == "__main__":
    main()