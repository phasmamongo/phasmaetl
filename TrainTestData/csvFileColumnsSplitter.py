import csv

import logging
import sys
from collections import defaultdict

import configproperties

logging.basicConfig(level = configproperties.loggingLevel)
logger = logging.getLogger(__name__)




def columnsSplitter(path):
  try:
        # averages = []
        # wavelenths = []
        columnsData = []

        columns = defaultdict(list)  # each value in each column is appended to a list

        with open(path, encoding="utf8") as f :

            csv_reader = csv.reader(f)
            firstLine = next(csv_reader)
            # print(firstLine)

        with open(path, encoding="utf8") as f:

             reader = csv.DictReader(f)  # read rows into a dictionary format

             for row in reader:  # read a row as {column1: value1, column2: value2,...}

                for (k, v) in row.items():  # go over each column name and value

                        columns[k].append(v)  # append the value into the appropriate list
                        # based on column name k

             for i in range(len(firstLine)):
                 # if i == 0 :
                 #     wavelenths = columns[firstLine[i]]
                 # elif i == 1 :
                 #     averages = columns[firstLine[i]]
                 if i>1 :
                     columnsData.append(columns[firstLine[i]])

        return columnsData
            # ,wavelenths,averages

  except:

      logger.error(" * Exception in " + path)
      logger.error(sys.exc_info()[0])
      columnsData = ""
      # wavelenths = ""
      # averages = ""

      return  columnsData
        # ,wavelenths,averages




