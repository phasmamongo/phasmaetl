import configproperties
import random
import logging

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)


def intNumOfMeasurements(percent, whole):
    testSamplesNumber = int((percent * whole) / 100.0)
    if whole <= 1 :
        return 0
    else:
        if testSamplesNumber == 0 :
            return 1
        else:
            return testSamplesNumber

def trainTestData(measurements):
    try :
        end = len(measurements)
        # print(end)
        howMany = intNumOfMeasurements(configproperties.testDataPercentage, end)
        # print(howMany)
        start = 1
        randomNumbers = random.sample(range(start, end + 1), howMany)

        logger.debug("%s" %randomNumbers)
        # print(randomNumbers)
        trainData = []
        testData = []
        for i in range(end):

            if i + 1 in randomNumbers:
                testData.append(measurements[i])
            else:
                trainData.append(measurements[i])

        return trainData,testData



    except:
           trainData = ""
           testData = ""
           return trainData,testData