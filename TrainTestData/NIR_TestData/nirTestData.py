import csv
import os

import sys

import configproperties
import logging
import datetime
from MongoDataLoading import jnames

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)

def createNirTestDataUc1(document, testDataWorkingDir):
    try:

        # print("")
        # print("              NIR              ")
        # print("")

        wavenumbers = document[jnames.experiment][configproperties.nirSensorName][jnames.wavenumbers]
        if not os.path.exists(testDataWorkingDir):
            os.makedirs(testDataWorkingDir)
        finalPath = os.path.join(testDataWorkingDir, document[jnames.useCase],
                                 document[jnames.laboratory],
                                 document[jnames.foodType],
                                 document[jnames.replicate],
                                 document[jnames.sample],
                                 document[jnames.temperature],
                                 document[jnames.tempExposureHours],
                                 configproperties.nirSensorName
                                 )

        if not os.path.exists(finalPath):
            os.makedirs(finalPath)

        countSample = 0
        for nirMeasurementDict in document[jnames.experiment][configproperties.nirSensorName][jnames.testData]:
            countSample += 1
            samplePath = os.path.join(finalPath, configproperties.testDataName + "_" + str(countSample)  + configproperties.csvSuffix)
            # print("Test Measurement : " + str(nirMeasurementDict))

            myFile = open(samplePath, 'w')
            with myFile:
                writer = csv.writer(myFile, lineterminator='\n')
                writer.writerows(zip(wavenumbers,  nirMeasurementDict))


        # print("White Reference : " + str(document[jnames.experiment][configproperties.nirSensorName][jnames.whiteReference]))
        samplePath = os.path.join(finalPath, jnames.whiteReference + "forAll" + configproperties.csvSuffix)
        myFile = open(samplePath, 'w')
        with myFile:
            writer = csv.writer(myFile, lineterminator='\n')
            writer.writerows(zip(wavenumbers, document[jnames.experiment][configproperties.nirSensorName][
                jnames.whiteReference]))


    except:

        logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                              :-3] + " NIR data not available")

    #     # exit(-1)

    # break




def createNirTestDataUc2(document,  testDataOutputDir):
    try:

        # print("")
        # print("              NIR              ")
        # print("")

        wavenumbers = document[jnames.experiment][configproperties.nirSensorName][jnames.wavenumbers]
        if not os.path.exists(testDataOutputDir):
            os.makedirs(testDataOutputDir)
        finalPath = os.path.join(testDataOutputDir, document[jnames.useCase],
                                 document[jnames.laboratory],
                                 document[jnames.foodType],
                                 document[jnames.replicate],
                                 document[jnames.sample],
                                 document[jnames.temperature],
                                 document[jnames.tempExposureHours],
                                 configproperties.nirSensorName
                                 )

        if not os.path.exists(finalPath):
            os.makedirs(finalPath)

        countSample = 0
        for nirMeasurementDict in document[jnames.experiment][configproperties.nirSensorName][jnames.testData]:
            countSample += 1
            samplePath = os.path.join(finalPath, configproperties.testDataName + "_" + str(countSample) + configproperties.csvSuffix)
            intensity = nirMeasurementDict[jnames.intensity]
            absorbance = nirMeasurementDict[jnames.absorbance]

            # print("Test Measurement : " + str(nirMeasurementDict))
            darkIntensity = document[jnames.experiment][configproperties.nirSensorName][jnames.darkIntensity]

            myFile = open(samplePath, 'w')
            with myFile:
                writer = csv.writer(myFile, lineterminator='\n')
                writer.writerow([configproperties.csvDarkIntensity + darkIntensity])
                writer.writerow([configproperties.csvWavenumber,configproperties.csvIntensityHeader,configproperties.csvAbsorbanceHeader])
                writer.writerows(zip(wavenumbers,  intensity ,absorbance))


        # print("White Reference : " + str(document[jnames.experiment][configproperties.nirSensorName][jnames.whiteReference]))
        samplePath = os.path.join(finalPath, jnames.whiteReference + "_" + "forAll" + configproperties.csvSuffix)
        myFile = open(samplePath, 'w')
        with myFile:
                writer = csv.writer(myFile, lineterminator='\n')
                writer.writerows(zip(wavenumbers, document[jnames.experiment][configproperties.nirSensorName][
                    jnames.whiteReference]))
        # print("Dark Intensity  : " + str(document[jnames.experiment][configproperties.nirSensorName][jnames,darkIntensity]))


        # print("******************************************")

    except Exception as e :

        logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                              :-3] + " Nir data not available")

        exc_type, exc_obj, exc_tb = sys.exc_info()

        logger.error(" * Exception in " )
        logger.error(sys.exc_info()[0])
        logger.error(e)

        logger.error(exc_type)
        logger.error(exc_tb.tb_lineno)



    #     # exit(-1)

    # break