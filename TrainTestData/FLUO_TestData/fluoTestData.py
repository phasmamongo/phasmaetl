import csv
import os
import configproperties
import logging
import datetime
from MongoDataLoading import jnames

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)

def createFluoTestData(document, testDataOutputDir) :
    try:

        # print("")
        # print("              FLUO              ")
        # print("")
        wavelengths = document[jnames.experiment][configproperties.fluoSensorName][jnames.wavelengths]

        if not os.path.exists(testDataOutputDir) :
            os.mkdir(testDataOutputDir)

        finalPath = os.path.join(testDataOutputDir, document[jnames.useCase],
                                 document[jnames.laboratory],
                                 document[jnames.foodType],
                                 document[jnames.replicate],
                                 document[jnames.sample],
                                 document[jnames.temperature],
                                 document[jnames.tempExposureHours],
                                 configproperties.fluoSensorName
                                 )


        if not os.path.exists(finalPath) :
            os.makedirs(finalPath)




        countSample = 0

        for fluoMeasurementDict in document[jnames.experiment][configproperties.fluoSensorName][jnames.measurements][
            jnames.testData]:
            countTestData = 0
            countDarkTestData = 0
            countSample +=1

            sampleAveragePath = os.path.join(finalPath,configproperties.averageTestDataName + "_" + str(countSample) + configproperties.csvSuffix)
            myFile = open(sampleAveragePath, 'w')
            with myFile:
                writer = csv.writer(myFile, lineterminator='\n')
                writer.writerow([configproperties.csvWavelengthHeader, configproperties.csvValueHeaeder])
                writer.writerows(zip(wavelengths, fluoMeasurementDict[jnames.averageData][jnames.measurements]))

            sampleAverageDarkPath = os.path.join(finalPath, configproperties.averageDarkTestDataName + "_" + str(countSample) + configproperties.csvSuffix)
            myFile = open(sampleAverageDarkPath, 'w')
            with myFile:
                writer = csv.writer(myFile, lineterminator='\n')
                writer.writerow([configproperties.csvWavelengthHeader, configproperties.csvValueHeaeder])
                writer.writerows(zip(wavelengths, fluoMeasurementDict[jnames.averageData][jnames.dark]))

            for testMeasurement in fluoMeasurementDict[jnames.columnsData][jnames.measurements]:
                countTestData += 1
                samplePath = os.path.join(finalPath,configproperties.testDataName + "_" + str(countSample) +"_" + str(countTestData) + configproperties.csvSuffix)

                # print("Test Measurement     : " + str(testMeasurement))
                myFile = open(samplePath, 'w')
                with myFile:
                    writer = csv.writer(myFile,lineterminator='\n')
                    writer.writerow([configproperties.csvWavelengthHeader, configproperties.csvValueHeaeder])
                    writer.writerows(zip(wavelengths,testMeasurement))
            for testDarkMeasurement in fluoMeasurementDict[jnames.columnsData][jnames.dark]:
                countDarkTestData += 1
                sampleDarkPath = os.path.join(finalPath, configproperties.testDarkDataName + "_" + str(countSample) + "_" + str(countDarkTestData) + configproperties.csvSuffix)

                # print("Test Dark Measurement : " + str(testDarkMeasurement))
                myFile = open(sampleDarkPath, 'w')
                with myFile:
                    writer = csv.writer(myFile, lineterminator='\n')
                    writer.writerow([configproperties.csvWavelengthHeader, configproperties.csvValueHeaeder])
                    writer.writerows(zip(wavelengths, testDarkMeasurement))

            # print("******************************************")

    except:

        logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                              :-3] + " Fluo date not available")