import csv
import os
import configproperties
import logging
import datetime
from MongoDataLoading import jnames

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)

def createVisTestData(document, testDataOutputDir) :
    try:

        wavelengths = document[jnames.experiment][configproperties.uvVisSensorName][jnames.wavelengths]
        if not os.path.exists(testDataOutputDir):
            os.makedirs(testDataOutputDir)

        finalPath = os.path.join(testDataOutputDir, document[jnames.useCase],
                                 document[jnames.laboratory],
                                 document[jnames.foodType],
                                 document[jnames.replicate],
                                 document[jnames.sample],
                                 document[jnames.temperature],
                                 document[jnames.tempExposureHours],
                                 configproperties.uvVisSensorName
                                 )


        if not os.path.exists(finalPath):
            os.makedirs(finalPath)

        countSample = 0

        for visMeasurementDict in document[jnames.experiment][configproperties.uvVisSensorName][jnames.measurements][
            jnames.testData]:
            countTestData = 0
            countDarkTestData = 0
            countRefTestData = 0
            countSample += 1

            sampleAveragePath = os.path.join(finalPath, configproperties.averageTestDataName + "_" + str(countSample) + configproperties.csvSuffix)
            myFile = open(sampleAveragePath, 'w')
            with myFile:
                writer = csv.writer(myFile, lineterminator='\n')
                writer.writerow([configproperties.csvWavelengthHeader, configproperties.csvValueHeaeder])
                writer.writerows(zip(wavelengths, visMeasurementDict[jnames.averageData][jnames.measurements]))

            sampleAverageDarkPath = os.path.join(finalPath, configproperties.averageDarkTestDataName + "_" + str(countSample) + configproperties.csvSuffix)
            myFile = open(sampleAverageDarkPath, 'w')
            with myFile:
                    writer = csv.writer(myFile, lineterminator='\n')
                    writer.writerow([configproperties.csvWavelengthHeader, configproperties.csvValueHeaeder])
                    writer.writerows(zip(wavelengths, visMeasurementDict[jnames.averageData][jnames.dark]))

            sampleAverageRefPath = os.path.join(finalPath, configproperties.averageRefTestDataName + "_" + str(countSample) + configproperties.csvSuffix)
            myFile = open(sampleAverageRefPath, 'w')
            with myFile:
                writer = csv.writer(myFile, lineterminator='\n')
                writer.writerow([configproperties.csvWavelengthHeader, configproperties.csvValueHeaeder])
                writer.writerows(zip(wavelengths, visMeasurementDict[jnames.averageData][jnames.ref]))



            for testMeasurement in visMeasurementDict[jnames.columnsData][jnames.measurements]:
                countTestData += 1
                samplePath = os.path.join(finalPath, configproperties.testDataName + "_" + str(countSample) + "_" + str(countTestData) + configproperties.csvSuffix)

                # print("Test Measurement      : " + str(testMeasurement))
                myFile = open(samplePath, 'w')

                with myFile:
                    writer = csv.writer(myFile, lineterminator='\n')
                    writer.writerow([configproperties.csvWavelengthHeader, configproperties.csvValueHeaeder])
                    writer.writerows(zip(wavelengths, testMeasurement))

            for testDarkMeasurement in visMeasurementDict[jnames.columnsData][jnames.dark]:
                countDarkTestData += 1
                sampleDarkPath = os.path.join(finalPath, configproperties.testDarkDataName + "_" + str(countSample) + "_" + str(countDarkTestData) + configproperties.csvSuffix)

                # print("Test Dark Measurement : " + str(testDarkMeasurement))
                myFile = open(sampleDarkPath, 'w')
                with myFile:
                    writer = csv.writer(myFile, lineterminator='\n')
                    writer.writerow([configproperties.csvWavelengthHeader, configproperties.csvValueHeaeder])
                    writer.writerows(zip(wavelengths, testDarkMeasurement))

            for testRefMeasurement in visMeasurementDict[jnames.columnsData][jnames.ref]:
                countRefTestData += 1
                sampleRefPath = os.path.join(finalPath, configproperties.testRefDataName + "_" + str(countSample) + "_" + str(countRefTestData) + configproperties.csvSuffix)

                # print("Test ref Measurement  : " + str(testRefMeasurement))
                myFile = open(sampleRefPath, 'w')
                with myFile:
                    writer = csv.writer(myFile, lineterminator='\n')
                    writer.writerow([configproperties.csvWavelengthHeader, configproperties.csvValueHeaeder])
                    writer.writerows(zip(wavelengths, testRefMeasurement))


    except:

        logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                              :-3] + " Vis date not available")
