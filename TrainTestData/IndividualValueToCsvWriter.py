import csv
import os
import configproperties
import logging
import datetime
from MongoDataLoading import jnames

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)

def createMicroToCsv(document, testDataOutputDir):
    try:

        if not os.path.exists(testDataOutputDir):
            os.mkdir(testDataOutputDir)

        finalPath = os.path.join(testDataOutputDir, document[jnames.useCase],
                                 document[jnames.laboratory],
                                 document[jnames.foodType],
                                 document[jnames.replicate],
                                 document[jnames.sample],
                                 document[jnames.temperature],
                                 document[jnames.tempExposureHours],
                                 configproperties.microbFolderName
                                 )

        if not os.path.exists(finalPath) :
            os.makedirs(finalPath)

        sampleMicroPath = os.path.join(finalPath, configproperties.microFileName + configproperties.csvSuffix)

        myFile = open(sampleMicroPath, 'w')
        with myFile:
             writer = csv.writer(myFile, lineterminator='\n')
             writer.writerow([jnames.microbiologicalValue, jnames.microbiologicalUnit])
             writer.writerow([document[jnames.microbiologicalMeasurement][jnames.microbiologicalValue],
                              document[jnames.microbiologicalMeasurement][jnames.microbiologicalUnit]])

    except:

        logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                              :-3] + " Microbiological value not available")

def createAflaToCsv(document, testDataOutputDir):
        try:

            if not os.path.exists(testDataOutputDir):
                os.mkdir(testDataOutputDir)

            finalPath = os.path.join(testDataOutputDir, document[jnames.useCase],
                                     document[jnames.laboratory],
                                     document[jnames.foodType],
                                     document[jnames.replicate],
                                     document[jnames.sample],
                                     document[jnames.temperature],
                                     document[jnames.tempExposureHours],
                                     configproperties.aflatoxinFolderName
                                     )

            if not os.path.exists(finalPath):
                os.makedirs(finalPath)

            sampleMicroPath = os.path.join(finalPath, configproperties.aflatoxinFileName + configproperties.csvSuffix)

            myFile = open(sampleMicroPath, 'w')
            with myFile:
                writer = csv.writer(myFile, lineterminator='\n')
                writer.writerow([jnames.aflaToMongoName, jnames.aflaValue, jnames.aflaToMongoUnit])
                writer.writerow([document[jnames.aflatoxin][jnames.aflaToMongoName], document[jnames.aflatoxin][
                    jnames.aflaToMongoValue],
                                 document[jnames.aflatoxin][jnames.aflaToMongoUnit]])


        except:

            logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                                  :-3] + " Aflatoxin value not available")