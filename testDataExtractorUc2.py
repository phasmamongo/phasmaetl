from pymongo import MongoClient

import configproperties
from TrainTestData.NIR_TestData import nirTestData
from TrainTestData.VIS_TestData import visTestData
import urllib.parse
from TrainTestData import IndividualValueToCsvWriter

if __name__ == '__main__':
    userName = urllib.parse.quote_plus(configproperties.mongoUserName)
    password = urllib.parse.quote_plus(configproperties.mongoPass)
    # client = MongoClient('mongodb://%s:%s@127.0.0.1' % (userName, password))
    client = MongoClient("localhost", 27017, maxPoolSize=50)

    db = client[configproperties.measurementsDbName]
    collection = db[configproperties.samplesCollectionName]


    cursor = collection.find({"useCase" : configproperties.uc2Name})

    count = 0

    for document in cursor:

        if count != 0 and count % 50 == 0:
            print("")

        print(">", end="", flush=True)

        count+= 1

        visTestData.createVisTestData(document, configproperties.testDataOutputDir)
        nirTestData.createNirTestDataUc2(document, configproperties.testDataOutputDir)
        IndividualValueToCsvWriter.createMicroToCsv(document, configproperties.testDataOutputDir)
    print("")
    print(str(count))
