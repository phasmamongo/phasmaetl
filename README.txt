PhasmaFOOD ETL Tool

In order to use the Python ETL tool, some changes of the parameters from the 'configproperties.py' file are required. 
The changes are refering to the 'uc1WorkingDir','uc2WorkingDir','uc3WorkingDir' variables. These are the basic directories 
for each use case created in accordance with the accepted folder structure(see below)  

Folder Structure

For each use case directory there are folders containing the data source definition and the data sample. Each folder has a 
specific name that contains information. The folders are being displayed according to the folder tree :
�	Use Case (UseCase1, UseCase2, UseCase3), 
�	Lab (WAG, AUA, BARI),
�	Food Type (Almond, Maize),
�	Replicate (Replicate1, Replicate2),
�	SampleID(Sample1, Sample2, A, B),
�	Temperature (4C, 8C, 12C, Dynamic),
�	Exposure Hours (0h, 14h, 24h, 34h, Dynamic),
�	Sensor (FLUO, NIR, MSI_IMG, VIS, MICRO, AFLATOXIN)


In addition, after request of the IP of the MongoDB, the Username and the Password, the user who wants to use the ETL tool 
is required to change these parameters as well in the 'configproperties.py' file.

Once these major changes are in place, the user has to execute the 'uc#DataLoader.py' file in order to upload the data 
gathered for the use case '#'.

