import logging

# Directory to work with. Must be the UseCaseN directory

uc1WorkingDir=r"/home/phasmamongo/dataAlt/Intra_UseCase_1v0.15NoPic"
uc2WorkingDir=r"/home/phasmamongo/dataAlt/Intra_UseCase_2v0.13NoPic"
uc3WorkingDir=r"/home/phasmamongo/dataAlt/Intra_UseCase_3v0.4"
testDataOutputDir= r"/home/phasmamongo/dataAlt/testDataOut"

mongoDbIp="x.x.x.x"
mongoDbPort=27017
mongoUserName = "xxxxxxxx"
mongoPass = "xxxxxxxxx"

testDataName = "testData"
averageTestDataName = "average_testData"
testDarkDataName = "testDarkData"
averageDarkTestDataName = "averageDark_testData"
testRefDataName = "testRefData"
averageRefTestDataName = "averageRef_testData"

csvWavelengthHeader = "Wavelength[nm]"
csvValueHeaeder = "Value"
csvWavenumber = "# Raw wavenumber"
csvIntensityHeader = "Raw intensity"
csvAbsorbanceHeader = "Raw absorbance"
csvDarkIntensity = "# DARK_INTENSITY="


measurementsDbName="measurements"
samplesCollectionName="AltJsonSamples"

uc1Name="UseCase1"
uc2Name="UseCase2"
uc3Name="UseCase3"

darkSuffix="_Dark"
whiteSuffix="_ref"
refSuffix="_ref"

csvSuffix=".csv"

averageFileName="Average"

uc1MicrobUnit=r"Total mesophiles log cfu/g"

fluoSensorName="FLUO"
ftirSensorName="FTIR"
msiImgSensorName="MSI_IMG"
nirSensorName="NIR"
uvVisSensorName="VIS"
microbFolderName="MICRO"
aflatoxinFolderName="AFLATOXIN"
aflatoxinFileName="aflatoxin"
microFileName="micro"


#if there is only 1 sample no test data will be produced
#for 2 samples and above(even for low perecentages) atleast 1 test sample will be provided
# in general integer division is performed to calculate the number or the test samples.
#what remains are the training samples
testDataPercentage=30




# Index range (begin and end) of columns to be kept
# E.g If you want to remove the first column and then every column after the fifth, set 1 to 5 keeping
# in mind that Python indexes start at 0
fluoAverageCsvProducerColumnBegin=1
fluoAverageCsvProducerColumnEnd=2
fluoAverageCsvProducerMeasurementRows=288


# Index range (begin and end) of columns to be kept
# E.g If you want to remove the first column and then every column after the fifth, set 1 to 5 keeping
# in mind that Python indexes start at 0
uvVisAverageCsvProducerColumnBegin=1
uvVisAverageCsvProducerColumnEnd=2
uvVisAverageCsvProducerMeasurementRows=288

# Index range (begin and end) of columns to be taken in account for the whiteDarkPreprocessor
# E.g If you want to remove the first column and then every column after the fifth, set 1 to 5 keeping
# in mind that Python indexes start at 0
whiteDarkPreprocessorColumnBegin=1
whiteDarkPreprocessorColumnEnd=2
whiteDarkPreprocessorOutFilePrefix="Preprocessed_"
whiteDarkPreprocessorPreprocessedValueColumnName="Preprocessed Value"

uc2NirDarkIntensityLine=4

uc2NirWhiteReferencePrefix="whiteref_"
uc2NirAverageCsvProducerNirSkipRows=21
uc2NirAverageCsvProducerMeasurementRows=901
uc2NirAverageCsvProducerColumnBegin=1
uc2NirAverageCsvProducerColumnEnd=3


uc1NirWhiteReferencePrefix="whiteref_"
uc1NirAverageCsvProducerNirSkipRows=0
uc1NirAverageCsvProducerMeasurementRows=901
uc1NirAverageCsvProducerColumnBegin=1
uc1NirAverageCsvProducerColumnEnd=4

# The number of token in the folder path that is considered to be the one just before the
# sensor folders appear (starting with the Lab folder as the 1st)
lastPathTokenNum=6

# After a csv has been loaded to the DB the file is moved to the Processed Files folder
# At every run a sub-folder is created with the current date to track daily operations
processedFilesFolder="ProcessedCsvFiles"

# Logging level that will be used
loggingLevel=logging.INFO

# Error message for wrong or missing date
wrongOrMissingDateErrorMsg=" : Date missing from Replicate folder name or in wrong format (dd-mm-yyyy)"



