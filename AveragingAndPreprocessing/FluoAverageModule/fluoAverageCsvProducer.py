import csv
import datetime
import fnmatch
import logging
import os

import numpy as np
import sys

import configproperties

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)


def listCsvFiles(path):
    return fnmatch.filter(os.listdir(path), '*.csv')

def produceAverage(dirPath):

    try:
        workingDir = dirPath
        # workingDir = configproperties.workingDir

        logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                             :-3] + " Processing started for " + workingDir)

        csvFilesList = listCsvFiles(workingDir)

        count = 0

        measAverageFloatArray = np.zeros(configproperties.fluoAverageCsvProducerMeasurementRows)
        # whiteAverageFloatArray = np.zeros(configproperties.fluoAverageCsvProducerMeasurementRows)
        # darkAverageFloatArray = np.zeros(configproperties.fluoAverageCsvProducerMeasurementRows)

        for csvFile in csvFilesList:
            # logger.debug(" * Processing file: " + csvFile)

            if str.isdigit(csvFile[-5]):
                # found a base filename
                # print(csvFile[:-4])

                count += 1

                pathToMeasCsv = os.path.join(workingDir, csvFile)
                # pathToDarkCsv = os.path.join(workingDir, csvFile[:-4] + configproperties.darkSuffix + ".csv")
                # pathToWhiteCsv = os.path.join(workingDir, csvFile[:-4] + configproperties.whiteSuffix + ".csv")

                begin = configproperties.fluoAverageCsvProducerColumnBegin
                end = configproperties.fluoAverageCsvProducerColumnEnd

                # with open(pathToWhiteCsv,  "r") as whiteFileIn:
                #     with open(pathToDarkCsv, "r") as darkFileIn:
                with open(pathToMeasCsv, "r") as measFileIn:

                    firstPass=True
                    rowNum = 0
                    # darkReader = csv.reader(darkFileIn)
                    # whiteReader = csv.reader(whiteFileIn)

                    for currentRefRow in csv.reader(measFileIn):

                        # darkFileIn.seek(0)
                        # whiteFileIn.seek(0)

                        if firstPass:
                            firstPass = False
                            rowNum+= 1
                            continue

                        # print(">>> Current Row Number : " + str(rowNum))
                        # currentDarkRow = next(itertools.islice(darkReader, rowNum, None))
                        # currentWhiteRow = next(itertools.islice(whiteReader, rowNum, None))

                        measAverageFloatArray[rowNum-1] += float(currentRefRow[begin:end][0])
                        # whiteAverageFloatArray[rowNum-1] += float(currentDarkRow[begin:end][0])
                        # darkAverageFloatArray[rowNum-1] += float(currentWhiteRow[begin:end][0])

                        rowNum+= 1

        # measAverageFloatArray[:] = [x / configproperties.filesToAverage for x in
        #                             measAverageFloatArray]
        # whiteAverageFloatArray[:] = [x / configproperties.filesToAverage for x in
        #                              whiteAverageFloatArray]
        # darkAverageFloatArray[:] = [x / configproperties.filesToAverage for x in
        #                             darkAverageFloatArray]

        if len(csvFilesList) != 0 and count != 0:

            measAverageFloatArray[:] = [x / count for x in measAverageFloatArray]
            # whiteAverageFloatArray[:] = [x / count for x in whiteAverageFloatArray]
            # darkAverageFloatArray[:] = [x / count for x in darkAverageFloatArray]

            # Use this to write column names and wavelengths
            pathToFirstCsv = os.path.join(workingDir, csvFilesList[0])

            # Output files with averages
            pathToMeasAverageOutCsv = os.path.join(workingDir, configproperties.averageFileName + ".csv")
            # pathToDarkAverageOutCsv = os.path.join(workingDir, configproperties.averageFileName + configproperties.darkSuffix + ".csv")
            # pathToWhiteAverageOutCsv = os.path.join(workingDir, configproperties.averageFileName + configproperties.whiteSuffix + ".csv")

            with open(pathToFirstCsv, "r") as firstCsvIn:
                # with open(pathToDarkAverageOutCsv, "w", newline='') as darkAverageFileOut:
                with open(pathToMeasAverageOutCsv, "w", newline='') as measAverageFileOut:
                    # with open(pathToWhiteAverageOutCsv, "w",
                    #           newline='') as whiteAverageFileOut:  # make this dir a local temp one

                    # darkAverageWriter = csv.writer(darkAverageFileOut)
                    measAverageWriter = csv.writer(measAverageFileOut)
                    # whiteAverageWriter = csv.writer(whiteAverageFileOut)

                    # row_count = sum(1 for row in measFileIn)  # fileObject is your csv.reader
                    # print (row_count)

                    firstPass = True
                    rowNum = 0

                    for currentSampleRow in csv.reader(firstCsvIn):

                        if firstPass:
                            firstPass = False
                            rowNum += 1
                            # darkAverageWriter.writerow(currentSampleRow[0:2])
                            measAverageWriter.writerow(currentSampleRow[0:2])
                            # whiteAverageWriter.writerow(currentSampleRow[0:2])
                            continue

                        measAverageVal = measAverageFloatArray[rowNum - 1]
                        # darkAverageVal = darkAverageFloatArray[rowNum - 1]
                        # whiteAverageVal = whiteAverageFloatArray[rowNum - 1]

                        measAverageRow = currentSampleRow[0:1]
                        # darkAverageRow = currentSampleRow[0:1]
                        # whiteAverageRow = currentSampleRow[0:1]

                        measAverageRow.append(str(measAverageVal))
                        # darkAverageRow.append(str(darkAverageVal))
                        # whiteAverageRow.append(str(whiteAverageVal))

                        measAverageWriter.writerow(measAverageRow)
                        # darkAverageWriter.writerow(darkAverageRow)
                        # whiteAverageWriter.writerow(whiteAverageRow)

                        rowNum += 1

        logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[:-3] + " Processing complete.")
    except:
        logger.error(" * Exception in " + workingDir)
        logger.error(sys.exc_info()[0])
