import csv
import datetime
import fnmatch
import logging
import os

import numpy as np
import sys

import configproperties

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)


def listCsvFiles(path):
    return fnmatch.filter(os.listdir(path), '*.csv')

def produceAverage(dirPath):

    try:
        nirWorkingDir = dirPath
        # workingDir = configproperties.workingDir

        logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                             :-3] + " Processing started for " + nirWorkingDir)

        csvFilesList = listCsvFiles(nirWorkingDir)

        count = 0

        averIntensityFloatArray = np.zeros(configproperties.uc2NirAverageCsvProducerMeasurementRows)
        averAbsorbanceFloatArray = np.zeros(configproperties.uc2NirAverageCsvProducerMeasurementRows)

        # print(csvFilesList)
        for csvFile in csvFilesList:
            # logger.debug(" * Processing file: " + csvFile)
            if (configproperties.uc2NirWhiteReferencePrefix not in csvFile):

                count += 1

                pathToMeasCsv = os.path.join(nirWorkingDir, csvFile)

                begin = configproperties.uc2NirAverageCsvProducerColumnBegin
                end = configproperties.uc2NirAverageCsvProducerColumnEnd

                with open(pathToMeasCsv, "r") as measFileIn:

                    rowNum = 0

                    for currentRefRow in csv.reader(measFileIn):

                        if rowNum < configproperties.uc2NirAverageCsvProducerNirSkipRows:
                            rowNum+=1
                            continue

                        # print(currentRefRow)
                        # print(str(rowNum))
                        averIntensityFloatArray[rowNum - configproperties.uc2NirAverageCsvProducerNirSkipRows] += float(currentRefRow[begin:end][0])
                        averAbsorbanceFloatArray[rowNum - configproperties.uc2NirAverageCsvProducerNirSkipRows] += float(currentRefRow[begin:end][1])
                        # print(str(float(currentRefRow[begin:end][1])))
                        rowNum+= 1

        if count != 0:

            averIntensityFloatArray[:] = [x / count for x in averIntensityFloatArray]
            averAbsorbanceFloatArray[:] = [x / count for x in averAbsorbanceFloatArray]

            pathToFirstCsv = ""

            for csvFile in csvFilesList:
                # logger.debug(" * Processing file: " + csvFile)
                if (configproperties.uc2NirWhiteReferencePrefix not in csvFile):
                    pathToFirstCsv = os.path.join(nirWorkingDir, csvFile)
                    break

            # Output files with averages
            pathToAverageOutCsv = os.path.join(nirWorkingDir, configproperties.averageFileName + ".csv")

            with open(pathToFirstCsv, "r") as firstCsvIn:
                with open(pathToAverageOutCsv, "w", newline='') as averageFileOut:

                    averageWriter = csv.writer(averageFileOut)

                    # row_count = sum(1 for row in measFileIn)  # fileObject is your csv.reader
                    # print (row_count)

                    rowNum = 0

                    for currentSampleRow in csv.reader(firstCsvIn):

                        if rowNum < configproperties.uc2NirAverageCsvProducerNirSkipRows:
                            averageWriter.writerow(currentSampleRow)
                            rowNum += 1
                            continue

                        averIntensityVal = averIntensityFloatArray[rowNum - configproperties.uc2NirAverageCsvProducerNirSkipRows]
                        averAbsorbanceVal = averAbsorbanceFloatArray[rowNum - configproperties.uc2NirAverageCsvProducerNirSkipRows]

                        averageRow = currentSampleRow[0:1]

                        averageRow.append(str(averIntensityVal))
                        averageRow.append(str(averAbsorbanceVal))

                        averageWriter.writerow(averageRow)

                        rowNum += 1

        logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[:-3] + " Processing complete.")
        # print(str(count))
    except:
        logger.error(" * Exception in " + nirWorkingDir)
        logger.error(sys.exc_info()[0])





