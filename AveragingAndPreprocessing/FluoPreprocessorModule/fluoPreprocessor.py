import csv
import itertools
import logging
import os

import sys

import configproperties

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)


def processFolder(dirPath):

    # logger.info("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[:-3] + " Processing started for " + configproperties.useCaseName)
    # logger.info("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[:-3] + " Processing complete for " + configproperties.useCaseName)
    # logger.debug(" * Processing file: " + csvFile)

    try:
        # workingDir = configproperties.workingDir
        workingDir = dirPath

        pathToMeasCsv = os.path.join(workingDir, configproperties.averageFileName + ".csv")
        pathToDarkCsv = os.path.join(workingDir, configproperties.averageFileName + configproperties.darkSuffix + ".csv")
        pathToWhiteCsv = os.path.join(workingDir, configproperties.averageFileName + configproperties.whiteSuffix + ".csv")

        if os.path.exists(pathToMeasCsv) and os.path.exists(pathToDarkCsv) and os.path.exists(pathToWhiteCsv):


            pathToOutCsv = os.path.join(workingDir, configproperties.whiteDarkPreprocessorOutFilePrefix + configproperties.averageFileName + ".csv")

            begin = configproperties.whiteDarkPreprocessorColumnBegin
            end = configproperties.whiteDarkPreprocessorColumnEnd

            with open(pathToWhiteCsv,  "r") as whiteFileIn:
                with open(pathToDarkCsv, "r") as darkFileIn:
                    with open(pathToMeasCsv, "r") as measFileIn:
                        with open(pathToOutCsv,  "w", newline='') as fileOut: # make this dir a local temp one
                            writer = csv.writer(fileOut)

                            # row_count = sum(1 for row in measFileIn)  # fileObject is your csv.reader
                            # print (row_count)

                            firstPass=True
                            rowNum = 0
                            darkReader = csv.reader(darkFileIn)
                            whiteReader = csv.reader(whiteFileIn)

                            for currentRefRow in csv.reader(measFileIn):

                                darkFileIn.seek(0)
                                whiteFileIn.seek(0)

                                if firstPass:
                                    firstPass = False
                                    rowNum+= 1
                                    writer.writerow(currentRefRow[0:2])
                                    continue

                                # print(">>> Current Row Number : " + str(rowNum))
                                currentDarkRow = next(itertools.islice(darkReader, rowNum, None))
                                currentWhiteRow = next(itertools.islice(whiteReader, rowNum, None))

                                # print("Ref   : " + str(currentRefRow[begin:end]))
                                # print("Dark  : " + str(currentDarkRow[begin:end]))
                                # print("White : " + str(currentWhiteRow[begin:end]))

                                # print("----------------------")

                                # Extract the part of the row we are interested in and access the specific item
                                measVal = float(currentRefRow[begin:end][0])
                                darkVal = float(currentDarkRow[begin:end][0])
                                whiteVal = float(currentWhiteRow[begin:end][0])

                                procVal = (measVal - darkVal)/whiteVal

                                procRow = currentRefRow[0:1]
                                procRow.append(str(procVal))

                                writer.writerow(procRow)
                                rowNum+= 1

        else:
            logger.debug(" * Check average file is present for " + pathToMeasCsv)
            logger.debug(" * Check average file is present for " + pathToDarkCsv)
            logger.debug(" * Check average file is present for " + pathToWhiteCsv)
    except:
        logger.error(" * Exception in " + workingDir)
        logger.error(sys.exc_info()[0])


