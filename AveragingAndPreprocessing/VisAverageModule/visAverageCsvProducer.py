import csv
import datetime
import fnmatch
import logging
import os
import sys

import numpy as np

import configproperties

logging.basicConfig(level=configproperties.loggingLevel)
logger = logging.getLogger(__name__)


def listCsvFiles(path):
    return fnmatch.filter(os.listdir(path), '*.csv')


def csvToArray(floatArray, pathToCsv, begin, end):

    with open(pathToCsv, "r") as fileIn:

        rowNum = 0

        for currentfileRow in csv.reader(fileIn):

            if rowNum == 0:
                rowNum += 1
                continue

            floatArray[rowNum - 1] += float(currentfileRow[begin:end][0])
            rowNum += 1

def produceAverage(dirPath):

    try:
        workingDir = dirPath
        # workingDir = configproperties.workingDir

        logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[
                             :-3] + " Processing started for " + workingDir)

        csvFilesList = listCsvFiles(workingDir)

        count = 0

        measAverageFloatArray = np.zeros(configproperties.uvVisAverageCsvProducerMeasurementRows)
        whiteAverageFloatArray = np.zeros(configproperties.uvVisAverageCsvProducerMeasurementRows)
        darkAverageFloatArray = np.zeros(configproperties.uvVisAverageCsvProducerMeasurementRows)

        for csvFile in csvFilesList:
            # logger.debug(" * Processing file: " + csvFile)

            if str.isdigit(csvFile[-5]):
                # found a base filename
                # print(csvFile[:-4])

                count += 1

                pathToMeasCsv = os.path.join(workingDir, csvFile)
                pathToDarkCsv = os.path.join(workingDir, csvFile[:-4] + configproperties.darkSuffix + ".csv")
                pathToWhiteCsv = os.path.join(workingDir, csvFile[:-4] + configproperties.whiteSuffix + ".csv")

                begin = configproperties.uvVisAverageCsvProducerColumnBegin
                end = configproperties.uvVisAverageCsvProducerColumnEnd

                np.zeros(configproperties.uvVisAverageCsvProducerMeasurementRows)

                csvToArray(measAverageFloatArray, pathToMeasCsv, begin, end)
                csvToArray(whiteAverageFloatArray, pathToWhiteCsv, begin, end)
                csvToArray(darkAverageFloatArray, pathToDarkCsv, begin, end)

        if len(csvFilesList) != 0:

            measAverageFloatArray[:] = [x / count for x in measAverageFloatArray]
            whiteAverageFloatArray[:] = [x / count for x in whiteAverageFloatArray]
            darkAverageFloatArray[:] = [x / count for x in darkAverageFloatArray]

            # Use this to write column names and wavelengths
            pathToFirstCsv = os.path.join(workingDir, csvFilesList[0])

            # Output files with averages
            pathToMeasAverageOutCsv = os.path.join(workingDir, configproperties.averageFileName + ".csv")
            pathToDarkAverageOutCsv = os.path.join(workingDir, configproperties.averageFileName + configproperties.darkSuffix + ".csv")
            pathToWhiteAverageOutCsv = os.path.join(workingDir, configproperties.averageFileName + configproperties.whiteSuffix + ".csv")


            with open(pathToFirstCsv, "r") as firstCsvIn:
                with open(pathToDarkAverageOutCsv, "w", newline='') as darkAverageFileOut:
                    with open(pathToMeasAverageOutCsv, "w", newline='') as measAverageFileOut:
                        with open(pathToWhiteAverageOutCsv, "w",
                                  newline='') as whiteAverageFileOut:  # make this dir a local temp one

                            darkAverageWriter = csv.writer(darkAverageFileOut)
                            measAverageWriter = csv.writer(measAverageFileOut)
                            whiteAverageWriter = csv.writer(whiteAverageFileOut)

                            firstPass = True
                            rowNum = 0

                            for currentSampleRow in csv.reader(firstCsvIn):

                                if firstPass:
                                    firstPass = False
                                    rowNum += 1
                                    darkAverageWriter.writerow(currentSampleRow[0:2])
                                    measAverageWriter.writerow(currentSampleRow[0:2])
                                    whiteAverageWriter.writerow(currentSampleRow[0:2])
                                    continue

                                measAverageVal = measAverageFloatArray[rowNum - 1]
                                darkAverageVal = darkAverageFloatArray[rowNum - 1]
                                whiteAverageVal = whiteAverageFloatArray[rowNum - 1]

                                measAverageRow = currentSampleRow[0:1]
                                darkAverageRow = currentSampleRow[0:1]
                                whiteAverageRow = currentSampleRow[0:1]

                                measAverageRow.append(str(measAverageVal))
                                darkAverageRow.append(str(darkAverageVal))
                                whiteAverageRow.append(str(whiteAverageVal))

                                measAverageWriter.writerow(measAverageRow)
                                darkAverageWriter.writerow(darkAverageRow)
                                whiteAverageWriter.writerow(whiteAverageRow)

                                rowNum += 1

        logger.debug("  * " + datetime.datetime.utcnow().strftime('%Y-%m-%d@%H:%M:%S.%f')[:-3] + " Processing complete.")
    except:
        logger.error(" * Exception in " + workingDir)
        logger.error(sys.exc_info()[0])



