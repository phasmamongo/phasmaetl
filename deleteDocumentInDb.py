import pymongo
import configproperties


client = pymongo.MongoClient(configproperties.mongoDbIp, configproperties.mongoDbPort)  # making a connection
db = client[configproperties.measurementsDbName]  # getting a database (test_database))
samples = db[configproperties.samplesCollectionName]

myquery = {"sample": "A", "laboratory": "AUA", "foodType": "Rocket", "useCase": "UseCase2", "replicate": "Replicate2 25-07-2017", "temperature": "4C", "tempExposureHours": "14h"}
if db.samples.find(myquery):
    results = samples.remove(myquery)
    # print(results)
